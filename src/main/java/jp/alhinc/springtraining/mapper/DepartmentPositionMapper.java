package jp.alhinc.springtraining.mapper;

import java.util.List;

import javax.swing.text.Position;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DepartmentPositionMapper {

	//部署・役職の情報を取得
	List<Position> findAll();

}
