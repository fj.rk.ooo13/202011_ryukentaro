package jp.alhinc.springtraining.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import jp.alhinc.springtraining.entity.Branch;

@Mapper
public interface BranchMapper {

	//支店の情報を取得
	List<Branch> findAll();

}
