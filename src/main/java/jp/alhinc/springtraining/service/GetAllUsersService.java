package jp.alhinc.springtraining.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class GetAllUsersService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	//全ユーザーの情報を取得
	public List<User> getAllUsers() {
		return mapper.findAll();
	}

	//ユーザーidを元にユーザーの情報を取得
	public EditUserForm getUsers(int id) {
		User getUser = mapper.getUser(id);
		EditUserForm form = new EditUserForm();

		//entityで取得してきた情報をformにコピー
		BeanUtils.copyProperties(getUser, form);
		return form;
	}
}




