package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class CreateUserService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	//ユーザーの情報をformからentityに詰め替える
	public int create(CreateUserForm form) {
		User entity = new User();
		entity.setLogin_id(form.getLogin_id());
		entity.setName(form.getName());
		entity.setBranch_id(form.getBranch_id());
		entity.setDepartment_position_id(form.getDepartment_position_id());
		//パスワードを暗号化し、entityに詰める
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getRegistrationPassword()));
		return mapper.create(entity);
	}

	public boolean createConfirmation(String login_id) {
		//登録してあるlogin_idを取得
		User alreadyLoginId = mapper.getLoginId(login_id);
		//入力したlogin_idをセット
		User newLoginId = new User();
		newLoginId.setLogin_id(login_id);
		//登録してあるlogin_idか確認
		if (alreadyLoginId == null) {
			return false;
		} else if (alreadyLoginId.getLogin_id().equals(newLoginId.getLogin_id())) {
			//現在のlogin_idと入力したlogin_idを比較
			return true;
		}
		return false;
	}
}