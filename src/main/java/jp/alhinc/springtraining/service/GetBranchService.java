package jp.alhinc.springtraining.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.Branch;
import jp.alhinc.springtraining.mapper.BranchMapper;

@Service
public class GetBranchService {

	@Autowired
	private BranchMapper mapper;

	@Transactional
	//支店の情報を取得
	public List<Branch> getBranch() {
		return mapper.findAll();
	}

}
